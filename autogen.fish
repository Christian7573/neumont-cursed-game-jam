#!/usr/bin/fish
set sprites_file sprites.txt
cd dist
echo "" > $sprites_file
for file in sprites/*.xml
	echo $file >> $sprites_file
end
