import { Graphics } from "pixi.js";
import { global } from "./index";
import { Drawable, DrawableMode } from "./objects";
import { DelegateFunc } from "./util";
import { moving, NOT_MOVING } from "./controls";

export class Blackout {
	background_day = new Graphics().beginFill(0xffe5e9f0).drawRect(0, 0, 1, 1).endFill();
	background_night = new Graphics().beginFill(0xff3b4252).drawRect(0, 0, 1, 1).endFill();

	constructor() {
		global.background.addChild(this.background_night);
		global.background.addChild(this.background_day);
		
		this.resize();
		window.addEventListener("resize", this.resize.bind(this));
		this.transition_to_day();
	}

	nightness = 0.0;

	onframe_default: DelegateFunc = (dt: number) => {
		this.background_night.alpha = dt * 0.003;		
	};
	onframe: DelegateFunc = this.onframe_default;

	transition_time = 500;
	transition_to_night() {
		this.background_day.visible = false;
		this.background_night.visible = true;
		this.onframe = (dt: number) => {
			this.onframe_default(dt);
			this.nightness += dt / this.transition_time;
			if (this.nightness >= 1) {
				this.nightness = 1;
				this.onframe = this.onframe_default;
			}
			this.apply_all();
		}
	}
	transition_to_day() {
		this.background_day.visible = true;
		this.background_day.alpha = 0;
		this.onframe = (dt: number) => {
			this.onframe_default(dt);
			this.nightness -= dt / this.transition_time;
			if (this.nightness <= 0) {
				this.nightness = 0;
				this.onframe = this.onframe_default;
				this.background_night.visible = false;
			}
			this.background_day.alpha = 1 - this.nightness;
			this.apply_all();
		};
	}

	force(is_night: boolean) {
		if (is_night) {
			this.background_night.visible = true;
			this.background_day.visible = false;
			this.nightness = 1.0;
		} else {
			this.background_night.visible = false;
			this.background_day.visible = true;
			this.nightness = 0.0;
		}
		this.apply_all();
	}

	apply_blackout(obj: Drawable) {
		switch (obj.drawable_mode) {
			case DrawableMode.Day:
				obj.drawable.alpha = 1 - this.nightness;
				break;
			case DrawableMode.Night:
				obj.drawable.alpha = this.nightness;
				break;

			case DrawableMode.Unaffected:
				obj.drawable.alpha = 1;
				break;
			
			default:
				throw new Error("How did we end up here? " + obj.drawable_mode);
		}
	}

	objects = new Set<Drawable>();
	apply_all() {
		for (const obj of this.objects) this.apply_blackout(obj);
	}

	resize() {
		this.background_day.width = window.innerWidth;
		this.background_night.width = window.innerWidth;
		this.background_day.height = window.innerHeight;
		this.background_night.height = window.innerHeight;
	}
}
