import { Graphics, Text, Container, Point, Sprite } from "pixi.js";
import { audio_files } from "./audio";
import { Scene } from "./scene";
import { global } from "./index";
import { Obj7573, Velocitable, Collidable, DrawableMode } from "./objects";
import { Box as SBox, Vector as SV, Response as SResponse, } from "sat";
import Player from "./player";
import { do_velocity, do_collision_things, transform_requested, transform_is_reset, AboutToTransformPulse } from "./controls";
import { enforce_max_dt, Stopwatch } from "./util";

export class Healthbar {
	max_health: number = 10;
	objective_health: number = 5;
	effective_health: number = 5;

	actual_container = new Container();
	container: Container;
	pulsing_container = new Container();
	background: Graphics;
	bar: Graphics;
	dying_bar: Graphics;
	text: Text;

	constructor() {
		this.background = new Graphics()
			.beginFill(0xffd8dee9)
			.drawRect(0,0,5,1)
			.endFill();
		this.bar = new Graphics()
			.beginFill(0xffa3be8c)
			.drawRect(0,0,1,1)
			.endFill();
		this.dying_bar = new Graphics()
			.beginFill(0xffbf616a)
			.drawRect(0,0,1,1)
			.endFill();
		this.text = new Text("Yesn't", {
			fill: "red",
			fontSize: 40,
			align: "center",
		});
		this.text.anchor.set(0.5);
		this.text.position.set(2.5,0.5);
		this.text.height = 1;

		this.container = new Container();
		this.container.addChild(this.background);
		this.container.addChild(this.dying_bar);
		this.container.addChild(this.bar);
		this.container.addChild(this.text);
		
		this.container.position.set(-2.5, -0.5);
		this.pulsing_container.addChild(this.container);
		this.actual_container.addChild(this.pulsing_container);

		this.resize();
		window.addEventListener("resize", this.resize.bind(this));
	}
	
	last_played_heartbeat_at = 5;
	onframe(dt: number) {
		if (this.effective_health > this.objective_health) {
			if (this.effective_health < this.last_played_heartbeat_at) {
				const sound = audio_files.get("heartbeat")!;
				sound.el.playbackRate = 1.4;
				sound.ensure_connected();
				sound.play_beginning();
				this.last_played_heartbeat_at -= 1;
			}
			this.effective_health = Math.max(this.effective_health - (dt * 0.002), this.objective_health);
			this.pulsing_container.scale.set(1 + ((this.effective_health % 1) * 0.25));
			this.do_bar();
		}

		if (this.effective_health === 0 && !(global.scene ?? { has_died: true }).has_died) new DeadScene();
	}

	do_bar() {
		this.dying_bar.width = this.effective_health / this.max_health * 5;
		this.bar.width = this.objective_health / this.max_health * 5;
		this.text.text = `${Math.floor(this.effective_health)}/${this.max_health}`;
		this.text.updateText(true);
		this.text.width = this.text.texture.width / this.text.texture.height * this.text.height * 0.6;
	}

	resize() {
		const wscale = Math.min(400, window.innerWidth - 40) / 5;
		const hscale = wscale * 0.2;
		this.actual_container.scale.set(wscale, hscale);
		this.actual_container.position.set(window.innerWidth / 2, window.innerHeight - (hscale * 0.5) - 20);
		this.do_bar();
	}

	set_health(health: number) {
		this.objective_health = Math.max(health, 0);		
		this.effective_health = Math.max(this.effective_health, this.objective_health);
		this.last_played_heartbeat_at = Math.max(this.last_played_heartbeat_at, this.objective_health);
		this.do_bar();
	}
	set_health_raw(health: number) {
		this.objective_health = health;
		this.effective_health = health;
		this.last_played_heartbeat_at = Math.floor(health);
		this.do_bar();
	}

	get health(): number { return this.objective_health }
	set health(value: number) { this.set_health(value); }
}

export class BloodBit extends Obj7573 implements Velocitable {
	readonly is_velocitable = true;
	velocity;
	obeys_gravity = true;
	on_ground = false;
	drawable_mode = DrawableMode.Unaffected;
	human_can_pass_through = true;

	constructor(position: Point, velocity: Point) {
		super(
			new Graphics()
				.beginFill(0xff990000)
				.drawRect(-0.05,-0.05,0.1,0.1)
				.endFill(),
			new SBox(new SV(-0.05,-0.05),0.1,0.1).toPolygon(),
			new Point(-0.05,-0.05)
		);
		this.position.copyFrom(position);
		this.position_updated();
		this.velocity = velocity.clone();
	}

	on_collision(other: Collidable, result: SResponse) {
		if ((other as any).human_can_pass_through) return true;
		this.velocity.set(
			this.velocity.x * (-result.overlapN.x !== 0 ? -result.overlapN.x : 1) + result.overlapV.x,
			this.velocity.y * (-result.overlapN.y !== 0 ? -result.overlapN.y : 1) + result.overlapV.y,
		);
		return false;
	}
}

export class DeadScene extends Scene {
	msg_container;
	red;
	dead_msg;
	the_scene;
	resize_func: any;
	started = new Stopwatch();
	transition_time = 1500;
	goal_time = this.started.ellapsed() + this.transition_time;
	local_height = 0;

	constructor() {
		global.scene.has_died = true;
		super({ theme: "lab", view_extends: [1,1,1,1], health: 8 });
		global.healthbar.max_health = global.scene.health;
		global.healthbar.set_health_raw(0);
		this.main_character = new Player(false);
		this.view_extents = global.scene.view_extents;
		global.main_character.drawable_mode = DrawableMode.Unaffected;
		global.blackout.apply_blackout(global.main_character);

		this.collidables = global.scene.collidables;
		this.collidables.delete(global.main_character);
		global.scene.drawable.addChild(this.drawable);

		this.msg_container = new Container();
		this.msg_container.alpha = global.main_character.is_muck_right_now ? 0.1 : 1;
		this.red = new Graphics()
			.beginFill(0xff330000)
			.drawRect(0, 0, 1, 1)
			.endFill();
		this.dead_msg = Sprite.from("/sprites/dead.png");
		this.dead_msg.anchor.set(0.5);
		this.dead_msg.width = 3 * 3;
		this.dead_msg.height = 1.5 * 3;
		this.msg_container.addChild(this.red);
		this.drawable.addChild(this.msg_container);
		(window as any).dead_msg = this.dead_msg;
		this.drawable.addChild(this.dead_msg);
		this.dead_msg.position.copyFrom(global.main_character.position);
		this.the_scene = global.scene;
		this.resize_func = this.resize.bind(this);		
		this.resize();

		global.main_character.drawable.parent.removeChild(global.main_character.drawable);
		this.drawable.addChild(global.main_character.drawable);
		const increment = Math.PI * 2 / 60;
		for (let i = 0; i < Math.PI * 2; i += increment) {
			const magnitude = Math.random() * 10;
			this.add_object(new BloodBit(global.main_character.position, new Point( Math.cos(i) * magnitude, Math.sin(i) * magnitude )));
		}

		global.delegate = this.delegate;
	}

	resize() {
		const scale_x = 1 / global.pulse.scale.x / global.scale.scale.x;
		const scale_y = 1 / global.pulse.scale.y / global.scale.scale.y;
		this.red.scale.x = window.innerWidth * scale_x;
		this.red.scale.y = window.innerHeight * scale_y * 2;
		this.msg_container.position.x = global.main_character.position.x - (this.red.scale.x / 2);
		this.local_height = this.red.scale.y;
		this.position();
		if (global.scene === this.the_scene) window.addEventListener("resize", this.resize_func, {once: true});
	}

	position() {
		const progress = Math.min(0, (this.started.ellapsed() - this.goal_time) / 500);
		this.msg_container.position.y = global.main_character.position.y - (this.local_height * 0.5) + (this.local_height * progress);
	}

	delegate_func(total_dt: number) {
		this.position();

		if (!transform_is_reset.v) transform_requested.v = false;
		if (transform_requested.v) {
			transform_requested.v = false;
			new AboutToTransformPulse();			
		}

		for (const dt of enforce_max_dt(total_dt, 50, 1000)) {
			for (const obj of this.velocitables) {
				do_velocity(obj, dt);
				if ((obj as any as Collidable).is_collidable) do_collision_things(obj as Velocitable & Collidable, this);
			}
		}
	}
}
