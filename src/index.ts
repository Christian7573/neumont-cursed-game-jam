import { Box, expose, Stopwatch } from "./util";
import { Container, Point, Graphics, Renderer, } from "pixi.js";
import { Scene, do_viewbox, load_and_transition } from "./scene";
import * as AllObjects from "./objects";
import { Blackout } from "./blackout";
import { Healthbar } from "./health";
import Player from "./player";
import XmlParser from "./xml";
import { load_all_the_things } from "./super_sprite";

import * as audio from "./audio";
expose("_audio", audio);

const renderer = new Renderer({
	backgroundAlpha: 1,
	backgroundColor: 0xff0000,
	clearBeforeRender: false,
	width: window.innerWidth,
	height: window.innerHeight,
	preserveDrawingBuffer: true,
	view: document.querySelector("canvas") as HTMLCanvasElement,
	useContextAlpha: true,
});
const stage = new Container();
const background = new Container(); 
stage.addChild(background);
const scale = new Container();
stage.addChild(scale);
const pulse = new Container();
scale.addChild(pulse);
const pan = new Container();
pulse.addChild(pan);

function resize_app() {
	renderer.resize(window.innerWidth, window.innerHeight);
	scale.position.set(window.innerWidth / 2, window.innerHeight / 2);
}
resize_app();
window.addEventListener("resize", resize_app);

export interface Global {
	renderer: Renderer;
	stage: Container;
	background: Container;
	scale: Container;
	pulse: Container;
	pan: Container;
	delegate: ((dt: number) => void);
	scene: Scene;
	blackout: Blackout;
	healthbar: Healthbar;
	main_character: Player;
	xml_parser: XmlParser;
}
export const global = {
	renderer,
	stage,
	background,
	scale,
	pulse,
	pan,
	healthbar: new Healthbar(),
	delegate: () => {},
} as any as Global;
expose("global", global);
global.blackout = new Blackout();
stage.addChild(global.healthbar.actual_container);

do_viewbox(5, 5, 10, 10);

const xml_parser = new XmlParser([
	Scene, Player, 
	...(Object.keys(AllObjects).map(key => (AllObjects as any)[key]).filter(obj => typeof obj === "function"))
]);
global.xml_parser = xml_parser;

load_all_the_things()
.then(() => { return new Promise(resolve => {
	document.querySelector("button")!.innerText = "Launch";
	document.querySelector("button")!.disabled = false;
	document.querySelector("button")!.addEventListener("click", resolve as any, { once: true });
})})
.then(() => {
	document.querySelector("div")!.style.display = "none";
	const level_results = /[\?\&]level=([^\&]+)/.exec(window.location.search);
	const level = level_results == null ? "/levels/tutorial1.xml" : level_results![1];
	load_and_transition(level);
});

const stopwatch = new Stopwatch();
function render() {
	const dt = stopwatch.restart();
	global.delegate(dt);
	global.blackout!.onframe(dt);
	global.healthbar.onframe(dt);
	renderer.render(stage, { clear: false });
	requestAnimationFrame(render);
};
requestAnimationFrame(render);
