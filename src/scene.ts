import { DisplayObject, Container, Graphics, Point, } from "pixi.js";
import { Drawable, Collidable, Velocitable, SceneHooks, Nearable, Interactable, } from "./objects";
import Player from "./player";
import { global } from "./index";
import { DelegateFunc, enforce_max_dt, string_to_enum, } from "./util";
import { do_collision_things, do_velocity, do_player_movement, requests_interact, transform_requested, AboutToTransformPulse } from "./controls";
import { start_ebony, stop_ebony, } from "./audio";
import SuperSprite from "./super_sprite";

export let last_viewbox: [number, number, number, number] = [0, 0, 1, 1];
export function do_viewbox(cx: number, cy: number, width: number, height: number) {
	global.pan.position.set(-cx, -cy);
	const scale_by_height = window.innerHeight / height;
	const scale_by_width = window.innerWidth / width;
	global.scale.scale.set(Math.min(scale_by_height, scale_by_width));
	last_viewbox = arguments as any;
}
window.addEventListener("resize", () => { do_viewbox(...last_viewbox); });

export enum SceneTheme {
	Lab,
	Wasteland,
}

export class Scene {
	drawable = new Container();
	drawables = new Set<Drawable>();
	collidables = new Set<Collidable>();
	velocitables = new Set<Velocitable>();
	on_empower_objs = new Set<SceneHooks>();
	on_depower_objs = new Set<SceneHooks>();
	on_draw_basically_objs = new Set<SceneHooks>();
	interactables = new Set<Interactable>();
	nearables = new Set<Nearable>();
	empowered = false;

	super_sprites = new Set<SuperSprite>();

	main_character: Player = null as any;
	theme: SceneTheme;
	scene_path: string | null = null;
	view_extents: [number, number, number, number] = [5, 5, 5, 5];
	health: number;
	start_at_night = false;
	has_died = false;

	constructor(vars: any = {}) {
		if (typeof vars.theme !== "string") throw new Error("No valid theme on scene");
		if (Array.isArray(vars.view_extents)) {
			if (vars.view_extents.length >= 2) {
				this.view_extents[0] = this.view_extents[2] = vars.view_extents[0];
				this.view_extents[1] = this.view_extents[3] = vars.view_extents[1];
				if (vars.view_extents.length >= 4) {
					this.view_extents[2] = vars.view_extents[2];
					this.view_extents[3] = vars.view_extents[3];
				}
			}
		}
		this.theme = string_to_enum(vars.theme, SceneTheme);		
		if (typeof vars.health !== "number") throw new Error("No valid health on scene");
		this.health = vars.health;
		if (vars.start_at_night) this.start_at_night = true;
	}
	xml_accept_child(child: any) {
		this.add_object(child);
		if (child instanceof Player) this.main_character = child;
	}

	add_object(obj: object) {
		if ((obj as Drawable).is_drawable) {
			this.drawables.add(obj as Drawable);
			this.drawable.addChild((obj as Drawable).drawable);
			global.blackout.objects.add(obj as Drawable);
			global.blackout.apply_blackout(obj as Drawable);
		}
		if ((obj as Collidable).is_collidable) this.collidables.add(obj as Collidable);
		if ((obj as Velocitable).is_velocitable) this.velocitables.add(obj as Velocitable);
		if ((obj as Interactable).is_interactable) this.interactables.add(obj as Interactable);
		if ((obj as Nearable).is_nearable) this.nearables.add(obj as Nearable);
		if ((obj as SceneHooks).on_scene_draw_basically != null) this.on_draw_basically_objs.add(obj as SceneHooks);
		if ((obj as SceneHooks).on_scene_added != null) (obj as SceneHooks).on_scene_added!(this);
		if ((obj as SceneHooks).on_scene_empower != null) {
			this.on_empower_objs.add(obj as SceneHooks);
			if (this.empowered) (obj as SceneHooks).on_scene_empower!(this);
		}
		if ((obj as SceneHooks).on_scene_depower != null) this.on_depower_objs.add(obj as SceneHooks);
	}
	remove_object(obj: object) {
		if ((obj as Drawable).is_drawable) {
			this.drawables.delete(obj as Drawable);
			this.drawable.removeChild((obj as Drawable).drawable);
			global.blackout.objects.delete(obj as Drawable);
		}
		if ((obj as Collidable).is_collidable) this.collidables.delete(obj as Collidable);
		if ((obj as Velocitable).is_velocitable) this.velocitables.delete(obj as Velocitable);
		this.interactables.delete(obj as Interactable);
		this.nearables.delete(obj as Nearable);
		if ((obj as SceneHooks).on_scene_draw_basically != null) this.on_draw_basically_objs.delete(obj as SceneHooks);
		if (this.empowered && (obj as SceneHooks).on_scene_depower != null) { (obj as SceneHooks).on_scene_depower!(this); }
		this.on_depower_objs.delete(obj as SceneHooks);
		this.on_empower_objs.delete(obj as SceneHooks);
		if ((obj as SceneHooks).on_scene_removed != null) (obj as SceneHooks).on_scene_removed!(this);
	}

	triggered_nearables = new Set<Nearable>();
	last_ground_y = 0;
	delegate_func(total_dt: number) {
		if (transform_requested.v) {
			transform_requested.v = false;
			new AboutToTransformPulse();			
		}


		for (const dt of enforce_max_dt(total_dt, 50, 1000)) {
			do_player_movement(this.main_character, dt, this);
			const was_on_ground = this.main_character.on_ground;
			for (const obj of this.velocitables) {
				do_velocity(obj, dt);
				if ((obj as any as Collidable).is_collidable) do_collision_things(obj as Velocitable & Collidable, this);
			}
			if (this.main_character.on_ground) {
				if (!was_on_ground) {
					const damage = Math.max(0, Math.floor((global.main_character.position.y - this.last_ground_y) / 4));
					if (damage > 0 && !this.main_character.is_muck_right_now) {
						global.healthbar.health -= damage;
						global.healthbar.effective_health = global.healthbar.health + 0.25;
					}
				}
				this.last_ground_y = this.main_character.position.y;
			}
		}

		const new_triggered_nearables = new Set<Nearable>();
		for (const obj of this.nearables) {
			if (
				Math.abs(this.main_character.position.x - obj.nearable_position.x) <= obj.nearable_distance &&
				Math.abs(this.main_character.position.y - obj.nearable_position.y) <= obj.nearable_distance
			) {
				if (!this.triggered_nearables.delete(obj)) obj.begin_nearable(this.main_character);
				new_triggered_nearables.add(obj);
			}
		}
		for (const obj of this.triggered_nearables) obj.end_nearable(this.main_character);
		this.triggered_nearables = new_triggered_nearables;
		if (requests_interact.v) {
			requests_interact.v = false;
			for (const obj of this.interactables) {
				if (
					Math.abs(this.main_character.position.x - obj.interactable_position.x) <= obj.interactable_distance &&
					Math.abs(this.main_character.position.y - obj.interactable_position.y) <= obj.interactable_distance
				) {
					obj.interact_with(this.main_character);
				}
			}
		}

		this.do_viewbox();
		for (const obj of this.on_draw_basically_objs) obj.on_scene_draw_basically!(total_dt, this);
		for (const super_sprite of this.super_sprites) super_sprite.advance_animation(total_dt);
	}
	get delegate(): DelegateFunc { return this.delegate_func.bind(this); }

	do_viewbox() {
		const vl = this.main_character.position.x - this.view_extents[3];
		const vt = this.main_character.position.y - this.view_extents[0];
		const vw = this.view_extents[1] + this.view_extents[3];
		const vh = this.view_extents[0] + this.view_extents[2];
		do_viewbox(vl + (vw * 0.5), vt + (vh * 0.5), vw, vh);
	}

	empower() {
		this.last_ground_y = this.main_character.position.y;
		global.blackout.force(this.start_at_night);
		if (this.start_at_night) { this.main_character.set_to_muck(); this.main_character.on_scene_draw_basically(0); }
		else this.main_character.set_to_human();
		global.pan.addChild(this.drawable);
		global.main_character = this.main_character;
		for (const obj of this.on_empower_objs) obj.on_scene_empower!(this);
		global.healthbar.max_health = this.health;
		global.healthbar.set_health_raw(this.health);
	}
	depower() {
		for (const obj of this.on_depower_objs) obj.on_scene_depower!(this);
		global.main_character = null as any;
		global.pan.removeChild(this.drawable);
	}

	reset() {
		if (this.scene_path == null) throw new Error("Cannot reset, scene from unknown source");
		load_and_transition(this.scene_path);
	}

	power_channels = new Map<string, Set<(val: boolean) => void>>();
}

export async function load_and_transition(scene_path: string, do_music = false) {
	let stop_music;
	if (do_music) stop_music = stop_ebony();
	await begin_transition();
	if (do_music) await stop_music;
	const scene_url = new URL(scene_path, (global.scene ?? {}).scene_path ?? window.location.href).href;
	const res =  await fetch(scene_url);
	const xml = await res.text();
	const my_scene = global.xml_parser.parse_from_text(xml);	
	if (!(my_scene instanceof Scene)) throw new Error("Scene xml was not instance of Scene");
	my_scene.scene_path = scene_url;
	if (global.scene != null) global.scene.depower();
	if (do_music) start_ebony();
	await new Promise(resolve => setTimeout(resolve, 750));
	global.scene = my_scene;
	global.scene.empower();
	global.scene.do_viewbox();
	await end_transition();
}

const transition_time = 250;
let current_transition_obj: [DisplayObject, Function] | null = null;
export function begin_transition(): Promise<void> {
	return new Promise((resolve, _reject) => {
		if (current_transition_obj != null) {
			global.stage.removeChild(current_transition_obj[0]);
			window.removeEventListener("resize", current_transition_obj[1] as any);
			current_transition_obj = null;
		}

		const graphics = new Graphics()
			.beginFill(0xff000000)
			.drawRect(0,0,1,1)
			.endFill();
		let total_dt = 0;
		function my_position_func() {
			graphics.position.x = window.innerWidth * -(1 - (total_dt / transition_time));
		}
		function my_resize_func() {
			graphics.width = window.innerWidth;
			graphics.height = window.innerHeight;
		}
		my_resize_func();
		window.addEventListener("resize", my_resize_func);

		current_transition_obj = [graphics, my_resize_func];
		global.stage.addChild(graphics);
		global.delegate = (dt: number) => {
			total_dt = Math.min(transition_time, total_dt + dt);
			my_position_func();
			if (total_dt >= transition_time) resolve();
		}
	});
}

export function end_transition(): Promise<void> {
	return new Promise((resolve, _reject) => {
		if (current_transition_obj == null) {
			throw new Error("Died of transitionn't");
		}
		const [graphics, my_resize_func] = current_transition_obj;

		let total_dt = 0;
		function my_position_func() {
			graphics.position.x = window.innerWidth * (total_dt / transition_time);
		}
		my_resize_func();
		setTimeout(() => { window.removeEventListener("resize", my_resize_func as any); }, transition_time);

		global.delegate = (dt: number) => {
			total_dt = Math.min(transition_time, total_dt + dt);
			my_position_func();
			if (total_dt >= transition_time) {
				global.delegate = global.scene.delegate;
				global.stage.removeChild(graphics);
				current_transition_obj = null;
				resolve();
			}
		}
	});
}
