import { Scene } from "./scene";
import { Collidable, Velocitable, SceneHooks, } from "./objects";
import Player from "./player";
import { Response, testPolygonPolygon } from "sat";
import { Point } from "pixi.js";
import { Box, Stopwatch } from "./util";
import { global } from "./index";
import { audio_files } from "./audio";

export function do_collision_things(me: Collidable & Velocitable, scene: Scene) {
	const response = new Response();
	const has_on_collision = me.on_collision != null;
	let collided = false;
	me.on_ground = false;
	for (const obj of scene.collidables) {
		if (obj !== me) {
			response.clear();
			if (testPolygonPolygon(me.polygon, obj.polygon, response)) {
				if (has_on_collision ? !me.on_collision!(obj, response) : true) {
					collided = true;
					me.polygon.pos.x -= response.overlapV.x;
					me.polygon.pos.y -= response.overlapV.y;
					me.velocity.x -= response.overlapV.x;
					me.velocity.y -= response.overlapV.y;
					if (response.overlapV.y > 0) me.on_ground = true;
				}
			}
		}
	}
	if (collided && me.on_after_collision != null) me.on_after_collision!();
}

const moving_threshold = 0.00;
export function is_main_character_moving() {
	return Math.abs(global.main_character.velocity.x) > moving_threshold && Math.abs(global.main_character.velocity.y) > moving_threshold;
}

export const GRAVITY = new Point(0, 16.5);
export function do_velocity(obj: Velocitable, dt: number) {
	const velmult = 0.001 * dt;
	if (obj.obeys_gravity) {
		obj.velocity.x += GRAVITY.x * velmult;
		obj.velocity.y += GRAVITY.y * velmult;
	};
	obj.position.x += obj.velocity.x * velmult;
	obj.position.y += obj.velocity.y * velmult;
	obj.position_updated();
}

let a_key = false;
let d_key = false;
export const NOT_MOVING = 0;
export const MOVING_LEFT = -1;
export const MOVING_RIGHT = 1;
export const moving = new Box(NOT_MOVING);
export const PLAYER_MAX_VEL = 8;
export const PLAYER_ACCEL = 90;
//export const PLAYER_JUMP_ACCEL = GRAVITY.y * -2.5;
export const PLAYER_JUMP_VEL = -9;
export const PLAYER_JUMP_TERMINATE_VEL = -2;
export const PLAYER_JUMP_TIME = 150;
export const jumping = new Box<number | null>(null);
let jump_cancelled = false;

export function do_player_movement(player: Player, dt: number, scene: Scene) {
	const horizontal_accel = PLAYER_ACCEL / 1000 * dt;
	if (moving.v === MOVING_LEFT) {
		player.velocity.x = Math.max(player.velocity.x - horizontal_accel, -PLAYER_MAX_VEL);
		player.drawable.scale.x = -1;
	} else if (moving.v === MOVING_RIGHT) {
		player.velocity.x = Math.min(player.velocity.x + horizontal_accel, PLAYER_MAX_VEL);
		player.drawable.scale.x = 1;
	} else {
		if (player.velocity.x > 0) {
			player.velocity.x = Math.max(player.velocity.x - horizontal_accel, 0);
		} else {
			player.velocity.x = Math.min(player.velocity.x + horizontal_accel, 0);
		}
	}

	if (jumping.v != null && (jumping.v > 0 || player.on_ground)) {
		jumping.v += dt;
		if (jumping.v <= PLAYER_JUMP_TIME) player.velocity.y = PLAYER_JUMP_VEL;
	}
	if (jump_cancelled) {
		player.velocity.y = Math.max(player.velocity.y, PLAYER_JUMP_TERMINATE_VEL);
		jump_cancelled = false;
	}
}

let active_pulse: AboutToTransformPulse | null = null;
export const transform_requested = new Box(false);
export const transform_is_reset = new Box(false);
export class AboutToTransformPulse implements SceneHooks {
	time_ellapsed = 0;
	animation_time = 800;
	on_scene_draw_basically(dt: number) {
		this.time_ellapsed += dt;
		if (!transform_is_reset.v) {
			global.main_character.use_sprite(this.my_sprite);
			global.main_character.active_sprite.advance_animation(dt);
		}
		if (this.time_ellapsed >= this.animation_time) {
			global.pulse.scale.set(1);
			if (!transform_is_reset.v) this.prev_active_sprite = this.my_sprite;
			this.on_scene_depower();
			if (transform_is_reset.v) {
				global.healthbar.max_health = 1;
				global.healthbar.set_health_raw(1);
				global.scene.reset();
			} else {
				global.main_character.transform_toggle();
			}
		} else {
			global.pulse.scale.set(1 + (this.time_ellapsed / this.animation_time * 0.25));
		}
	}

	scene: Scene;
	audio = audio_files.get(transform_is_reset.v ? "reset" : "transform_buildup")!;//.duplicate();
	prev_delegate;
	my_delegate;
	prev_active_sprite;
	my_sprite;
	constructor() {
		this.prev_delegate = global.delegate;
		this.scene = global.scene;
		global.scene.add_object(this);
		if (transform_is_reset.v) {
			this.audio.el.volume = 0.25;
		}
		this.audio.ensure_connected();
		this.audio.play_beginning();
		this.my_delegate = global.delegate = this.on_scene_draw_basically.bind(this);
		active_pulse = this;
		this.prev_active_sprite = global.main_character.active_sprite;
		this.my_sprite = (global.main_character.is_muck_right_now ? global.main_character.muck_transform : global.main_character.human_transform);
		this.my_sprite.dt = 0;
	}

	on_scene_depower() {
		this.scene.remove_object(this);
	}
	on_scene_removed() {
		active_pulse = null;
		global.pulse.scale.set(1);
		//this.audio.el.pause();
		//this.audio.destroy();
		if (global.delegate === this.my_delegate) global.delegate = this.prev_delegate;
		global.main_character.use_sprite(this.prev_active_sprite);
	}
}

export const w_key = new Box(false);
export const requests_interact = new Box(false);

const A_KEY = 65;
const D_KEY = 68;
const R_KEY = 50;
const SPACE_KEY = 32;
const W_KEY = 87;
const Q_KEY = 81;
let r_key = false;
window.addEventListener("keydown", e => {
	if (e.keyCode === A_KEY && !a_key) {
		moving.v = MOVING_LEFT;		
		a_key = true;
	} else if (e.keyCode === D_KEY && !d_key) {
		moving.v = MOVING_RIGHT;
		d_key = true;
	} else if (e.keyCode === R_KEY && !transform_requested.v && !r_key) {
		transform_requested.v = true;
		transform_is_reset.v = false;
		r_key = true;
	} else if (e.keyCode === Q_KEY && !transform_requested.v) {
		transform_requested.v = true;
		transform_is_reset.v = true;
	} else if (e.keyCode === SPACE_KEY && jumping.v == null) {
		jumping.v = 0;
	} else if (e.keyCode === W_KEY && !w_key.v) {
		w_key.v = true;
		requests_interact.v = true;
	}
});
window.addEventListener("keyup", e => {
	if (e.keyCode === A_KEY && a_key) {
		a_key = false;
		if (moving.v === MOVING_LEFT) moving.v = NOT_MOVING;
	} else if (e.keyCode === D_KEY && d_key) {
		d_key = false;
		if (moving.v === MOVING_RIGHT) moving.v = NOT_MOVING;
	} else if (e.keyCode === R_KEY && !transform_is_reset.v && r_key) {
		transform_requested.v = false;
		r_key = false;
		if (active_pulse != null) active_pulse.on_scene_depower();
	} else if (e.keyCode === Q_KEY && transform_is_reset.v) {
		transform_requested.v = false;
		if (active_pulse != null) active_pulse.on_scene_depower();
	} else if (e.keyCode === SPACE_KEY && jumping.v != null) {
		jumping.v = null;
		jump_cancelled = true;
	} else if (e.keyCode === W_KEY && w_key.v) {
		w_key.v = false;
		requests_interact.v = false;
	}
});
