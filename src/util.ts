export class Box<T> { constructor(v: T) { this.v = v; }; v: T; }
export function expose(name: string, value: any) { (window as any)[name] = value; };

export class Stopwatch {
	last_time = performance.now();
	start() { this.last_time = performance.now(); }
	ellapsed() { return performance.now() - this.last_time; }
	restart() {
		const val = this.ellapsed();
		this.start();
		return val;
	}
}

export type DelegateFunc = (dt: number) => void;

export function enforce_max_dt(actual_dt: number, max_step_dt: number, max_total_dt: number): number[] {
	const out = [];
	let my_dt = Math.min(actual_dt, max_total_dt);
	while (my_dt > 0) {
		out.push(Math.min(actual_dt, max_step_dt));
		my_dt -= max_step_dt;
	}
	return out;
}

export function string_to_enum(theme: string, e: object): any {
	const lower_theme = theme.toLowerCase();
	for (const something in e) {
		if (something.toLowerCase() === lower_theme) return (e as any)[something];
	}
	throw new Error(`Invalid theme ${theme}`);
}
