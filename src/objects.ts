import { DisplayObject, Point, Graphics, Container, Sprite, } from "pixi.js";
import { Scene, load_and_transition } from "./scene";
import { Polygon, Box as SBox, Vector as SV, Response as SResponse, } from "sat";
import { is_main_character_moving } from "./controls";
import { get_texture } from "./audio";
import Player from "./player";
import { global } from "./index";
import SuperSprite from "./super_sprite";
import { string_to_enum } from "./util";

export interface Positionable {
	position: Point;
	position_updated(): void;
	set_position(x: number, y: number): void;
}
export enum DrawableMode {
	Day,
	Night,
	Unaffected,
}
export interface Drawable {
	is_drawable: true;
	drawable: DisplayObject;
	drawable_mode: DrawableMode;
}
export interface Collidable {
	is_collidable: true;
	polygon: Polygon;
	on_collision?(other: Collidable, collision_response: SResponse): boolean;
	on_after_collision?(): void;
}
export interface Velocitable extends Positionable {
	is_velocitable: true;
	velocity: Point;
	on_ground: boolean;
	obeys_gravity: boolean;
}
export interface SceneHooks {
	on_scene_added?(scene: Scene): void;
	on_scene_removed?(scene: Scene): void;
	on_scene_draw_basically?(dt: number, scene: Scene): void;
	on_scene_empower?(scene: Scene): void;
	on_scene_depower?(scene: Scene): void;
}
export interface Interactable {
	is_interactable: true;
	interactable_distance: number;
	interactable_position: Point;
	interact_with(player: Player): void;
}
export interface Nearable {
	is_nearable: true;
	nearable_distance: number;
	nearable_position: Point;
	begin_nearable(player: Player): void;
	end_nearable(player: Player): void;
}


export abstract class Obj7573 implements Drawable, Collidable, Positionable {
	readonly is_drawable = true;
	readonly is_collidable = true;
	drawable: DisplayObject;
	polygon: Polygon;
	position: Point;
	drawable_mode = DrawableMode.Day;
	
	collidable_offset: Point;

	constructor(drawable: DisplayObject, collidable: Polygon, collidable_offset: Point) {
		this.drawable = drawable;
		this.polygon = collidable;
		this.position = this.drawable.position;
		this.collidable_offset = collidable_offset;
	}

	position_updated() {
		this.polygon.pos.x = this.position.x + this.collidable_offset.x;
		this.polygon.pos.y = this.position.y + this.collidable_offset.y;
	}
	set_position(x: number, y: number) {
		this.position.set(x, y);
		this.position_updated();
	}
	on_after_collision() {
		this.position.set(this.polygon.pos.x - this.collidable_offset.x, this.polygon.pos.y - this.collidable_offset.y);
	}
}

export class SpannableBlock extends Obj7573 implements SceneHooks {
	min: Point;
	max: Point;
	half_size: Point;
	declare drawable: SuperSprite;

	constructor(sprite: string, params: any) {
		if (!Array.isArray(params.spans) || params.spans.length < 4) throw new Error("Ground without valid spans");
		const min = new Point(Math.min(params.spans[0], params.spans[1]), Math.min(params.spans[2], params.spans[3]));
		const max = new Point(Math.max(params.spans[0], params.spans[1]), Math.max(params.spans[2], params.spans[3]));
		const half_size = new Point((max.x - min.x) * 0.5, (max.y - min.y) * 0.5);
		const polygon =	new SBox(new SV(min.x, min.y), half_size.x * 2, half_size.y * 2).toPolygon();
		polygon.translate(-half_size.x, -half_size.y);
		super(
			SuperSprite.make(sprite),
			polygon,
			new Point(0, 0),
		);
		this.min = min;
		this.max = max;
		this.half_size = half_size;
		this.drawable.width = half_size.x * 2;
		this.drawable.height = half_size.y * 2;
		if (!("anchor" in this.drawable.init_params)) this.drawable.anchor.set(0.5, 0.5);
		if (typeof params.rotate === "number") {
			this.drawable.angle = params.rotate;
			this.polygon.rotate(this.drawable.rotation);
		}
		if ("drawmode" in params) this.drawable_mode = string_to_enum(params.drawmode, DrawableMode);
		this.set_position(this.min.x + this.half_size.x, this.min.y + this.half_size.y);
	}

	on_scene_empower(scene: Scene) {
		this.drawable.on_scene_empower(scene);
	}
}

export class PositionedDrawable implements Drawable {
	readonly is_drawable = true;
	drawable_mode = DrawableMode.Day;
	drawable: DisplayObject;
	constructor(drawable: DisplayObject, params: any) {
		this.drawable = drawable;
		if (!Array.isArray(params.position) || params.position.length < 2) throw new Error("No valid position for PositionedDrawable");
		this.drawable.position.set(...params.position);
	}
}

export class Note extends PositionedDrawable implements Drawable, SceneHooks {
	constructor(params: any) {
		super(
			new Graphics().beginFill(0xff000000).drawRect(-0.5, -0.5, 1, 1),
			params
		);
	}

	on_scene_draw_basically(dt: number, scene: Scene) {

	}
}

export class SolidSprite extends SpannableBlock {
	constructor(params: any) {
		if (typeof params.sprite !== "string") throw new Error("Invalid sprite url");
		super(params.sprite, params);
	}
}
export class BackgroundSprite extends SolidSprite {
	constructor(params: any) {
		super(params);
		(this as any).is_collidable = false;
	}
}

export class LevelEnd extends PositionedDrawable implements Interactable {
	next_level: string;

	constructor(params: any) {
		if (typeof params.next_level !== "string") throw new Error("No next level for LevelEnd");
		super(
			 new Graphics()
				.beginFill(0xff00ff00)
				.drawRect(-0.5,-1,1,2)
				.endFill(),
			params
		);
		this.next_level = params.next_level;
		this.interactable_position = this.drawable.position;
	}
	drawable_mode = DrawableMode.Day;
	
	readonly is_interactable = true;
	interactable_distance = 1.5;
	interactable_position: Point;
	interact_with() {
		global.healthbar.max_health = 1;
		global.healthbar.set_health_raw(1);
		load_and_transition(this.next_level, true);
	}
}
export class LevelEndNight extends PositionedDrawable implements SceneHooks {
	drawable_mode = DrawableMode.Night;
	constructor(params: any) {
		super(
			new Graphics()
			.lineStyle({width: 0.1, color: 0xffffffff, alignment: 0})
			.drawRect(-0.5,-1,1,2),
			params
		);
	}

	on_scene_draw_basically() {
		this.drawable.visible = is_main_character_moving();
	}
}

export class Bar extends SpannableBlock {
	is_vertical = false;

	readonly muck_can_pass_through = true;
	readonly muck_slows_down = true;

	constructor(params: any) {
		super(params.vertical ? "bars_vert" : "bars_hor", params);
		if (params.is_vertical) this.is_vertical = true;
	}
}

export class Spikes extends SpannableBlock {
	is_vertical = false;

	readonly human_can_pass_through = true;
	readonly hurts_human = true;
	readonly muck_can_pass_through = true;
	readonly muck_slows_down = true;

	constructor(params: any) {
		super("spikes", params);
		if (params.is_vertical) this.is_vertical = true;
	}
}

export class Water extends SpannableBlock {
	readonly human_can_pass_through = true;
	readonly muck_can_pass_through = true;
	readonly is_water = true;

	constructor(params: any) {
		super("water", params);
	}
}

export class Switch extends PositionedDrawable implements Interactable, SceneHooks {
	drawable_mode = DrawableMode.Day;
	readonly is_interactable = true;
	interactable_position: Point;
	interactable_distance = 1.3;

	value = false;
	activates: string[];

	next_wire_start: Point;
	constructor(params: any) {
		if (!Array.isArray(params.activates) || params.activates.some((el: any) => typeof el !== "string")) throw new Error("Invalid activates on Switch");
		super(
			new Graphics()
				.beginFill(0xff88dd88)
				.drawRect(-0.5,-0.5,1,1)
				.endFill(),
			params
		);
		this.interactable_position = this.drawable.position;
		this.next_wire_start = this.drawable.position;
		this.activates = params.activates;
		if (params.starts_activated) this.value = true;
	}

	wire_nodes: WirePoint[] = [];
	xml_accept_child(obj: any) {
		if (obj instanceof WirePoint) {
			obj.start_point = this.next_wire_start;
			this.next_wire_start = obj.end_point;
			this.wire_nodes.push(obj);
		} else {
			throw new Error("Child of Switch not WirePoint");
		}
	}

	scene: Scene | null = null;
	on_scene_added(scene: Scene) {
		this.scene = scene;
		for (const wire of this.wire_nodes) this.scene.add_object(wire);
		this.scene.drawable.removeChild(this.drawable);
		this.scene.drawable.addChild(this.drawable);
	}
	on_scene_empower(_scene: Scene) {
		this.send_value();
	}

	send_value() {
		for (const channel of this.activates) {
			for (const receiver of this.scene!.power_channels.get(channel) ?? []) {
				receiver(this.value);
			}
		}
		for (const wire of this.wire_nodes) wire.active_graphics.visible = this.value;
	}

	interact_with() {
		this.value = !this.value;
		this.send_value();
	}
}
export class WirePoint implements Drawable, SceneHooks {
	readonly is_drawable = true;
	drawable_mode = DrawableMode.Day;
	drawable = new Container();

	start_point: Point | null = null;
	end_point: Point;
	inverted = false;

	constructor(params: any) {
		if (!Array.isArray(params.position) || params.position.length < 2) throw new Error("Invalid position on wire point");
		this.end_point = new Point(...params.position);
	}

	active_graphics: Graphics = null as any;
	
	on_scene_added(_scene: Scene) {
		this.drawable.removeChildren();
		const backdrop = new Graphics()
			.lineStyle({ width: 0.2, color: 0xff000055, alignment: 0.5 })
			.moveTo(this.start_point!.x, this.start_point!.y)
			.lineTo(this.end_point.x, this.end_point.y);
		let slope = (this.end_point.y - this.start_point!.y) / (this.end_point.x - this.start_point!.x);
		const hor_mult = this.end_point.x - this.start_point!.x >= 0 ? 1 : -1;
		const endpoint_size = 0.2;
		const active = new Graphics()
			.lineStyle({ width: 0.1, color: 0xffffff00, alignment: 0.5 })
			.moveTo(this.start_point!.x + (endpoint_size * hor_mult), this.start_point!.y + (endpoint_size * hor_mult * slope))
			.lineTo(this.end_point.x - (endpoint_size * hor_mult), this.end_point.y - (endpoint_size * hor_mult * slope));
		active.visible = false;
		this.active_graphics = active;
		this.drawable.addChild(backdrop, active);
	}
}

export class PowerProvider {
	values = new Map<string, boolean>();
	on_value_updated: (value: boolean) => void = () => {};
	powered = false;
	inverted = false;
	constructor(params: any) {
		if (params.power_inverted) this.inverted = true;
		if (!Array.isArray(params.requires) || params.requires.some((obj: any) => typeof obj !== "string")) throw new Error("Invalid requires on wire point");
		for (const requires of params.requires) this.values.set(requires, false);
	}
	value_updated() {
		this.powered = Array.from(this.values.values()).reduce((a, b) => a && b);
		if (this.inverted) this.powered = !this.powered;
		this.on_value_updated(this.powered);
	}
	on_scene_added(scene: Scene) {
		for (const requirement of this.values.keys()) {
			const channel = scene.power_channels.get(requirement) ?? new Set();
			channel.add((val: boolean) => {
				this.values.set(requirement, val);
				this.value_updated();
			});
			scene.power_channels.set(requirement, channel);
		}
	}
}

export class Door extends SpannableBlock implements SceneHooks {
	is_open = false;
	power;
	get human_can_pass_through(): boolean { return this.is_open; }
	get muck_can_pass_through(): boolean { return this.is_open; }
	graphics;

	constructor(params: any) {
		super("nothing", params);
		this.graphics = new Graphics()
			.beginFill(0xffaaaaaa)
			.drawRect(0, 0, 1, 1)
			.endFill();
		this.drawable.addChild(this.graphics);
		this.graphics.width = this.half_size.x * 2;
		this.graphics.height = this.half_size.y * 2;
		this.graphics.position.set(-this.half_size.x, -this.half_size.y);
		this.power = new PowerProvider(params);
		this.power.on_value_updated = (val) => {
			this.is_open = val;
		};
	}

	on_scene_added(scene: Scene) {
		this.power.on_scene_added(scene);
	}

	on_scene_draw_basically(dt: number) {
		this.graphics.height = Math.min(this.half_size.y * 2, Math.max(0, this.graphics.height + (dt * 0.003 * (this.is_open ? -1 : 1))));
	}
}

export class Computer extends BackgroundSprite implements Interactable, SceneHooks {
	activated = false;
	power;

	readonly is_interactable = true;
	interactable_distance;
	interactable_position;

	constructor(params: any) {
		params.sprite = "/objects/computer.png";
		super(params);
		this.power = new PowerProvider(params);
		this.power.on_value_updated = (val) => {};
		this.interactable_position = this.drawable.position;
		this.interactable_distance = this.half_size.x + 1.5;
	}

	scene: Scene | null = null;
	on_scene_added(scene: Scene) {
		this.scene = scene;
		this.power.on_scene_added(scene);
	}

	interact_with() {
		if (this.power.powered || this.activated) {
			this.activated = true;
			alert("Activated");
			for (const channel of this.scene!.power_channels.get("computer") ?? new Set()) channel(true);
		}
	}
}

export class Candle extends PositionedDrawable implements SceneHooks {
	declare drawable: Sprite;
	constructor(params: any) {
		super(
			new Sprite(get_texture("/objects/candle.png")),
			params
		);
		this.drawable.width = 0.25;
		this.drawable.height = 1;
		this.drawable.anchor.set(0.5, 0);
	}

	on_scene_added(scene: Scene) {
		scene.add_object(new CandleShimmer(this));
	}
}
export class CandleShimmer implements Drawable, SceneHooks {
	readonly is_drawable = true;
	drawable_mode = DrawableMode.Night;
	drawable = new Graphics()
		.beginFill(0xaaffffff)
		//.drawCircle(0,0,0.1)
		.drawRect(-0.125,0,0.25,0.1)
		.endFill();

	constructor(parent: Candle) {
		this.drawable.position = parent.drawable.position;
	}

	on_scene_draw_basically() {
		this.drawable.visible = Math.random() <= 0.5;
	}
}

export class MovableGroup implements Velocitable, SceneHooks {
	readonly is_velocitable = true;

	obeys_gravity = false;
	min = new Point(-100000, -100000);
	max = new Point(100000, 100000);

	ocelate = false;
	off_velocity = new Point(0, 0);
	on_velocity = new Point(0, 0);
	power;
	get velocity(): Point { return this.power.powered ? this.on_velocity : this.off_velocity }
	on_ground = false;

	children = new Set<Positionable>();
	initial_position = new Point(0,0);

	constructor(params: any) {
		this.power = new PowerProvider(params);
		if ("on_velocity" in params) this.on_velocity.set(...params.on_velocity);
		if ("off_velocity" in params) this.off_velocity.set(...params.off_velocity);
		if ("spans" in params) {
			this.min.set(Math.min(params.spans[0], params.spans[1]), Math.min(params.spans[2], params.spans[3]));
			this.max.set(Math.max(params.spans[0], params.spans[1]), Math.max(params.spans[2], params.spans[3]));
		}
		if ("initial_position" in params) {
			this.initial_position.set(...params.initial_position);
		}
		if (params.ocelate) this.ocelate = true;
		if (params.obeys_gravity) this.obeys_gravity = true;
	}

	on_scene_added(scene: Scene) {
		this.power.on_scene_added(scene);
		for (const child of this.children) { scene.add_object(child); }
		this.position.copyFrom(this.initial_position);
		this.position_updated();
	}

	position = new Point(0, 0);
	prev_position = new Point(0, 0);
	delta_position = new Point(0, 0);
	set_position(x: number, y: number) {
		this.position.set(x, y);
		this.position_updated();
	}
	position_updated() {
		this.position.x = Math.min(this.max.x, Math.max(this.min.x, this.position.x));
		this.position.y = Math.min(this.max.y, Math.max(this.min.y, this.position.y));
		this.delta_position.set(this.position.x - this.prev_position.x, this.position.y - this.prev_position.y);
		for (const child of this.children) {
			child.position.set(child.position.x + this.delta_position.x, child.position.y + this.delta_position.y);
			child.position_updated();
		}
		this.prev_position.copyFrom(this.position);
		if (this.ocelate) {
			if (this.position.x === this.min.x || this.position.x === this.max.x) this.velocity.x = this.velocity.x * -1;
			if (this.position.y === this.min.y || this.position.y === this.max.y) this.velocity.y = this.velocity.y * -1;
		}
	}

	xml_accept_child(obj: any) {
		if ("position" in obj && typeof obj.position_updated === "function") this.children.add(obj);
		else throw new Error("Child of MovableGroup was not Positionable");
	}
}

export class DeathBox extends SpannableBlock {
	constructor(params: any) { super("nothing", params); }
	is_death = true;
}
