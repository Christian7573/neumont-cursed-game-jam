import { expose } from "./util";
import { Texture, } from "pixi.js";

export const audio_context = new AudioContext();
expose("audio_context", audio_context);

window.addEventListener("click", () => {audio_context.resume()});
window.addEventListener("keypress", () => {audio_context.resume()});
let has_started_music = false;
function mystartmusic() {
	if (!has_started_music) {
		has_started_music = true;
		start_ebony();
	}
}
window.addEventListener("click", mystartmusic, {once: true});
window.addEventListener("keypress", mystartmusic, {once: true});

export class AudioFile {
	el: HTMLMediaElement;
	node: MediaElementAudioSourceNode;
	can_destroy: boolean;
	destroyed = false;
	constructor(el: HTMLMediaElement, can_destroy = false) {
		this.el = el;
		this.node = audio_context.createMediaElementSource(el as HTMLMediaElement);
		this.can_destroy = can_destroy;
	}

	duplicate(): AudioFile {
		const el = this.el.cloneNode(true);
		document.body.appendChild(el);
		return new AudioFile(el as HTMLMediaElement, true);
	}
	
	has_connected = false;
	ensure_connected() {
		if (!this.has_connected) {
			this.node.connect(audio_context.destination);
			this.has_connected = true;
		}
	}

	play_beginning() {
		if (this.destroyed) throw new Error("Audio Destroyed");
		this.el.currentTime = 0;
		this.el.play();
	}

	destroy() {
		if (!this.can_destroy) throw new Error("Cannot destroy this one");
		document.body.removeChild(this.el);
		if (!this.el.ended) this.el.pause();
		this.destroyed = true;
	}
}

export const audio_files = new Map<String, AudioFile>();
for (const el of document.querySelectorAll("audio")) {
	if (el.id === "") throw new Error("Audio IDn't");
	audio_files.set(el.id, new AudioFile(el));
}
expose("audio_files", audio_files);

console.log("Audio please");

export const textures = new Map<string, Promise<Texture>>();
export function get_texture(_url?: string): Texture {
	return Texture.from("/texturent.png");
	/*const texture = textures.get(url);
	if (texture == null) {
		const new_texture = Texture.from(url);
		textures.set(url, new_texture);
		return new_texture;
	}
	return texture;*/
}
export function get_texture_async(url: string): Promise<Texture> {
	const texture = textures.get(url);
	if (texture == null) {
		const new_texture = Texture.fromURL(url);
		textures.set(url, new_texture);
		return new_texture;
	} else {
		return texture;
	}
}

const intro = audio_files.get("ebony_intro")!;
intro.ensure_connected();
const loop = audio_files.get("ebony_loop")!;
loop.ensure_connected();
const outro = audio_files.get("ebony_outro")!;
outro.ensure_connected();
intro.el.volume = 0.5;
loop.el.volume = 0.5;
loop.el.loop = true;
outro.el.volume = 0.5;

export function wait_for_media_to_stop(el: HTMLMediaElement): Promise<void> {
	return new Promise((resolve, reject) => {
		if (el.paused) resolve();
		else el.addEventListener("pause", resolve as any, { once: true });
	});
}

let ebony_canceled: object | null = null;
export async function start_ebony() {
	const my_token = {};
	ebony_canceled = my_token;
	await intro.el.play();
	await wait_for_media_to_stop(intro.el);
	if (ebony_canceled === my_token) {
		loop.el.currentTime = 0;
		await loop.el.play();								
	}
}

export async function stop_ebony() {
	ebony_canceled = null;
	intro.el.pause();
	loop.el.pause();
	await outro.el.play();
	await wait_for_media_to_stop(outro.el);
}
