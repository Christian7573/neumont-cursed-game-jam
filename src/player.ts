import { Obj7573, Velocitable, DrawableMode, Collidable, SceneHooks, } from "./objects";
import { Point, Graphics, Container } from "pixi.js";
import { Box as SBox, Vector as SV } from "sat";
import { global } from "./index";
import SuperSprite from "./super_sprite";
import { Scene } from "./scene";

export const half_width = 0.5;
export const half_height = 1;

export class Player extends Obj7573 implements Velocitable, SceneHooks {
	readonly is_velocitable = true;
	velocity = new Point();
	obeys_gravity = true;
	on_ground = false;

	declare drawable: Container;
	drawable_mode = DrawableMode.Unaffected;
	human_idle;
	human_running;
	human_jump_start;
	human_jump_end;
	human_transform;
	muck_transform;
	muck_idle;
	muck_slide;
	muck_jump_start;
	muck_jump_end;

	main_character: boolean;
	is_muck_right_now = false;
	constructor(main_character: boolean) {
		super(
			new Container(),
			new SBox(new SV(-half_width,-half_height),half_width*2,half_height*2).toPolygon(),
			new Point(-half_width, -half_height),
		);
		this.main_character = main_character;
		this.human_idle = this.setup_sprite(SuperSprite.make("human_idle"));
		this.human_running = this.setup_sprite(SuperSprite.make("human_running"));
		this.human_jump_start = this.setup_sprite(SuperSprite.make("human_jump_start"));
		this.human_jump_end = this.setup_sprite(SuperSprite.make("human_jump_end"));
		this.human_transform = this.setup_sprite(SuperSprite.make("human_transform"));
		this.muck_transform = this.setup_sprite(SuperSprite.make("muck_transform"));
		this.muck_idle = this.setup_sprite(SuperSprite.make("muck_idle"));
		this.muck_slide = this.setup_sprite(SuperSprite.make("muck_slide"));
		this.muck_jump_start = this.setup_sprite(SuperSprite.make("muck_jump_start"));
		this.muck_jump_end = this.setup_sprite(SuperSprite.make("muck_jump_end"));

		this.use_sprite(this.human_idle);
	}

	static xml_factory(params: any): Player {
		const main_character = typeof params.main_character === "boolean" ? params.main_character : false;
		const player = new Player(main_character);
		if ("position" in params) player.set_position(params.position.x, params.position.y);
		if (params.starts_as_muck) player.transform_to_muck();
		return player;
	}

	transform_toggle() {
		if (this.is_muck_right_now) this.transform_to_human();
		else this.transform_to_muck();
	}
	set_to_human() {
		//this.human.visible = true;
		//this.muck.visible = false;
		this.is_muck_right_now = false;
	}
	set_to_muck() {
		//this.human.visible = false;
		//this.muck.visible = true;
		this.is_muck_right_now = true;
	}
	transform_to_human() {
		this.set_to_human();
		global.blackout!.transition_to_day();
		global.healthbar.set_health(Math.max(global.healthbar.health - 1, 0));
		/*let total_dt = 0;
		const prev_delegate = global.delegate;
		/*global.delegate = (dt: number) => {
			total_dt += dt;
			if (total_dt >= 1000) global.delegate = prev_delegate;
		};*/
	}
	transform_to_muck() {
		this.set_to_muck();
		global.blackout!.transition_to_night();
		global.healthbar.set_health(Math.max(global.healthbar.health - 1, 0));
		/*let total_dt = 0;
		const prev_delegate = global.delegate;
		/*global.delegate = (dt: number) => {
			total_dt += dt;
			if (total_dt >= 1000) global.delegate = prev_delegate;
		};*/
	}

	setup_sprite(sprite: SuperSprite): SuperSprite {
		sprite.height = 2.56;
		sprite.width = 1.96;
		sprite.anchor.set(0.5);
		sprite.scale.set(0.9);
		sprite.super_setup();
		sprite.visible = false;
		this.drawable.addChild(sprite);
		return sprite;
	}
	on_scene_empower(scene: Scene) {
		this.human_idle.on_scene_empower(scene);
		this.human_running.on_scene_empower(scene);
		this.human_jump_start.on_scene_empower(scene);
		this.human_jump_end.on_scene_empower(scene);
		this.human_transform.on_scene_empower(scene);
		this.muck_transform.on_scene_empower(scene);
		this.muck_idle.on_scene_empower(scene);
		this.muck_slide.on_scene_empower(scene);
		this.muck_jump_start.on_scene_empower(scene);
		this.muck_jump_end.on_scene_empower(scene);
	}

	on_collision(other: Collidable) {
		if ((other as any).is_death) {
			global.healthbar.set_health(0);
			global.healthbar.effective_health = 0.5;
		}
		if ((other as any).is_water && this.is_muck_right_now) global.healthbar.set_health(0);
		if ((other as any).hurts_human && !this.is_muck_right_now && global.healthbar.effective_health === global.healthbar.objective_health) global.healthbar.set_health(global.healthbar.objective_health - 1);

		if ((other as any).human_can_pass_through && !this.is_muck_right_now) {
			return true;
		}
		if ((other as any).muck_can_pass_through && this.is_muck_right_now) {
			if ((other as any).muck_shows_down) {
				if (this.velocity.x < 0) this.velocity.x = Math.max(this.velocity.x, -3); else this.velocity.x = Math.min(this.velocity.x, 3);
				if (this.velocity.y < 0) this.velocity.y = Math.max(this.velocity.y, -3); else this.velocity.y = Math.min(this.velocity.y, 3);
			}
			return true;
		}
		return false;
	}

	was_running = false;
	was_on_ground = true;
	was_going_up = false;
	declare active_sprite: SuperSprite;
	use_sprite(sprite: SuperSprite) {
		this.human_idle.visible = false;;
		this.human_running.visible = false;
		this.human_jump_start.visible = false;
		this.human_jump_end.visible = false;
		this.human_transform.visible = false;
		this.muck_transform.visible = false;
		this.muck_idle.visible = false;
		this.muck_slide.visible = false;
		this.muck_jump_start.visible = false;
		this.muck_jump_end.visible = false;
		sprite.visible = true;
		this.active_sprite = sprite;
	}
	on_scene_draw_basically(dt: number) {
		if (!this.on_ground) {
			const going_up = this.velocity.y < 0;
			if (!this.is_muck_right_now) this.use_sprite(going_up ? this.human_jump_start : this.human_jump_end);
			else this.use_sprite(this.muck_jump_start);
			if (this.was_on_ground) { this.human_jump_start.dt = 0; this.muck_jump_start.dt = 0; }
			if (this.was_going_up) { this.human_jump_end.dt = 0; this.muck_jump_end.dt = 0; }
			this.was_running = false;
			this.was_going_up = going_up;
		} else if (!this.was_on_ground || (!this.is_muck_right_now && this.human_jump_end.dt < 1125) || (this.is_muck_right_now && this.human_jump_end.dt < 260)) {
			if (!this.was_going_up) { this.human_jump_end.dt = 1000; this.was_going_up = true; }
			this.use_sprite(this.is_muck_right_now ? this.muck_jump_end : this.human_jump_end);			
		} else {
			const running = Math.abs(this.velocity.x) > 0;
			if (running) {
				const sprite = this.is_muck_right_now ? this.muck_slide : this.human_running;
				this.use_sprite(sprite);
				if (!this.was_running) sprite.dt = 0;
			} else {
				const sprite = this.is_muck_right_now ? this.muck_idle : this.human_idle;
				this.use_sprite(sprite);
				if (this.was_running) sprite.dt = 0;
			}
			this.was_running = running;
		}
		this.was_on_ground = this.on_ground;
	}
}
export default Player;
