export type XmlInflater<T> = (el: Element, parser: XmlParser) => T;

export function standard_inflater_factory(val: any) {
	return function(el: Element, parser: XmlParser): object {
		const params: any = {};								
		const children = [];
		for (const param of el.getAttributeNames()) {
			if (param.length < 2 || param.substring(0,2) !== "x-") {
				try { params[param] = JSON.parse(el.getAttribute(param)!); }
				catch (e) {
					console.error(el);
					console.error(e);
					throw new Error(`Failed to JSON parse attribute ${param} for element ${el.tagName}`);
				}
			}
		}
		for (const child of el.children) {
			const param = child.getAttribute("x-param");						
			const child_obj = parser.parse(child);
			if (param !== null) params[param] = child_obj;
			else children.push(child_obj);
		}
		const obj = typeof val.xml_factory === "function" ? val.xml_factory(params) : new val(params);
		if (typeof obj.xml_accept_child === "function") {
			for (const child of children) obj.xml_accept_child(child);
		} else if (children.length > 0) {
			console.error(el);
			console.error(obj);
			throw new Error(`Object ${el.tagName} does not accept children`);
		}
		return obj;
	}
}
export class XmlParser {
	types = new Map<string, XmlInflater<any>>();

	register_class(val: any): void;
	register_class(name: string, val: any): void;
	register_class(a: any,  b?: any): void {
		if (typeof a === "string") {
			this.types.set(a, standard_inflater_factory(b));
		} else if (typeof a.name !== "string") {
			throw new Error("Type didn't provide a .name property");
		} else {
			this.types.set(a.name, standard_inflater_factory(a));
		}
	}

	constructor(classes_to_register: any[], register_builtins = true) {
		for (const c of classes_to_register) this.register_class(c);
	}

	parse(el: Element) {
		const inflater = this.types.get(el.tagName);;
		if (el.tagName.toLowerCase() === "parsererror") {
			console.error(el);
			console.error((el as any).innerText);
			throw new Error("Invalid XML");
		}
		if (inflater == null) throw new Error(`${el.tagName} is not a registered type`);
		else return inflater(el, this);
	}

	parse_from_text(xml_text: string) {
		const root_el = (new DOMParser()).parseFromString(xml_text, "text/xml").documentElement;
		if (root_el.tagName.toLowerCase() === "html" || root_el.tagName.toLowerCase() === "parsererror") {
			console.error(root_el);
			console.error(root_el.innerText);
			throw new Error("Invalid XML");
		}
		return this.parse(root_el);
	}
}
export default XmlParser;
