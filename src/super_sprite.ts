import { Sprite, TilingSprite, Texture, SCALE_MODES } from "pixi.js";
import { string_to_enum, expose } from "./util";
import { get_texture, get_texture_async } from "./audio";
import { Scene } from "./scene";
import XmlParser from "./xml";

export enum RenderingMode {
	TilingLocked,
	TilingFree,
	StretchLocked,
	StretchFree
}

export class SuperSprite extends TilingSprite {
	x_mode = RenderingMode.StretchFree;
	y_mode = RenderingMode.StretchFree;
	id: string | null = null;
	init_params: any;

	dt = 0;
	raw_animation_frames: AnimationFrame[] = [];
	animation_frames: AnimationFrame[] = [];
	total_dt = 0;
	animation_times: number[] = [];

	top_decoration: SuperSprite | null = null;
	bottom_decoration: SuperSprite | null = null;
	left_decoration: SuperSprite | null = null;
	right_decoration: SuperSprite | null = null;

	constructor(params: any) {
		super(get_texture("/texturent.png"));
		this.init_params = params;
		if ("x_mode" in params) this.x_mode = string_to_enum(params.x_mode, RenderingMode);
		if ("y_mode" in params) this.y_mode = string_to_enum(params.y_mode, RenderingMode);
		if ("texture" in params) {
			this.raw_animation_frames.push(new AnimationFrame({ texture: params.texture, dt: -1 }));
			this.raw_animation_frames[0].parent = this;
		}
		if ("id" in params) this.id = params.id;
		if ("anchor" in params) this.anchor.set(...params.anchor);
		if ("angle" in params) this.angle = params.angle;

		const old_scale_cb = this.scale.cb;
		this.scale.cb = () => { this.super_setup(); old_scale_cb.call(this.scale.scope); };
		if (this.x_mode === RenderingMode.TilingLocked || this.x_mode === RenderingMode.StretchLocked
		 || this.y_mode === RenderingMode.TilingLocked || this.y_mode === RenderingMode.StretchLocked) {
			 const old_position_cb = this.position.cb;
			this.position.cb = (...args: any[]) => { this.super_setup(); old_position_cb(...(args as [])); };
		 }
	}
	xml_accept_child(obj: any) {
		if (obj instanceof AnimationFrame) {
			obj.parent = this;
			this.raw_animation_frames.push(obj);
		}
		else throw new Error("SuperSprite bad child");
	}

	super_setup() {
		//console.log([this._width, this._height, this.texture.width, this.texture.height, this.animation_frames.length]);
		//if (this.animation_frames.length === 9) throw new Error();
		switch (this.x_mode) {
			case RenderingMode.StretchFree: {
				this.tileScale.x = this._width / this.texture.width;
				this.tilePosition.x = 0;
				break;
			}
			case RenderingMode.StretchLocked: {
				const actual_min_x = this.position.x - (this._width * this.anchor.x);
				const min_x = Math.floor(actual_min_x);
				const max_x = Math.ceil(this.position.x + (this._width * (1 - this.anchor.x)));
				this.tileScale.x = (max_x - min_x) / this.texture.width;
				this.tilePosition.x = min_x - actual_min_x;
				break;
			}

			case RenderingMode.TilingFree: {
				this.tileScale.x = 1 / this.texture.width;
				this.tilePosition.x = 0;
				break;
			}

			case RenderingMode.TilingLocked: {
				const actual_min_x = this.position.x - (this._width * this.anchor.x);
				const min_x = Math.floor(actual_min_x);
				this.tileScale.x = 1 / this.texture.width;
				this.tilePosition.x = min_x - actual_min_x;
				break;
			}

			default:
				throw new Error("How did we get here?");
		}

		switch (this.y_mode) {
			case RenderingMode.StretchFree: {
				this.tileScale.y = this._height / this.texture.height;
				this.tilePosition.y = 0;
				break;
			}
			case RenderingMode.StretchLocked: {
				const actual_min_x = this.position.y - (this._height * this.anchor.y);
				const min_x = Math.floor(actual_min_x);
				const max_x = Math.ceil(this.position.y + (this._height * (1 - this.anchor.y)));
				this.tileScale.y = (max_x - min_x) / this.texture.height;
				this.tilePosition.y = min_x - actual_min_x;
				break;
			}

			case RenderingMode.TilingFree: {
				this.tileScale.y = 1 / this.texture.height;
				this.tilePosition.y = 0;
				break;
			}

			case RenderingMode.TilingLocked: {
				const actual_min_x = this.position.y - (this._height * this.anchor.y);
				const min_x = Math.floor(actual_min_x);
				this.tileScale.y = 1 / this.texture.height;
				this.tilePosition.y = min_x - actual_min_x;
				break;
			}

			default:
				throw new Error("How did we get here?");
		}

		if (this.top_decoration != null) {
			this.top_decoration.width = this._width;
			this.top_decoration.super_setup();
		}
		if (this.bottom_decoration != null) {
			this.bottom_decoration.width = this._width;
			this.bottom_decoration.super_setup();
		}
		if (this.left_decoration != null) {
			this.left_decoration.width = this._width;
			this.left_decoration.super_setup();
		}
		if (this.right_decoration != null) {
			this.right_decoration.width = this._width;
			this.right_decoration.super_setup();
		}
	}

	on_scene_empower(scene: Scene) {
		this.animation_frames = Array.from(this.raw_animation_frames);
		for (const animation_frame of this.animation_frames) {
			this.animation_times.push(animation_frame.dt + this.total_dt);
			this.total_dt += animation_frame.dt;
		}
		this.advance_animation(0);
		this.super_setup();
		scene.super_sprites.add(this);
		if (this.top_decoration != null) {
			this.top_decoration.position.set(0,0);
			this.addChild(this.top_decoration);
			this.top_decoration.on_scene_empower(scene);
		}
		if (this.bottom_decoration != null) {
			this.bottom_decoration.position.set(0,1);
			this.addChild(this.bottom_decoration);
			this.bottom_decoration.on_scene_empower(scene);
		}
		if (this.left_decoration != null) {
			this.left_decoration.position.set(0,0);
			this.addChild(this.left_decoration);
			this.left_decoration.on_scene_empower(scene);
		}
		if (this.right_decoration != null) {
			this.right_decoration.position.set(1,0);
			this.addChild(this.right_decoration);
			this.right_decoration.on_scene_empower(scene);
		}
	}
	advance_animation(dt: number) {
		this.dt += dt;
		if (this.dt > this.total_dt) {
			this.dt = this.dt % this.total_dt;
		}
		for (let i = 0; i < this.animation_times.length; i++) {
			if (this.dt <= this.animation_times[i]) {
				this.texture = this.animation_frames[i].texture;
				break;
			}
		}
	}

	static sprites = new Map<string, SuperSprite>();
	static make(id: string): SuperSprite {
		const sprite = SuperSprite.sprites.get(id);
		if (sprite != null) return sprite.clone();
		else throw new Error("Sprite not found " + id);
	}

	clone(): SuperSprite {
		delete this.init_params.texture;
		const sprite = new SuperSprite(this.init_params);		
		sprite.raw_animation_frames = Array.from(this.raw_animation_frames);
		sprite.width = this._width;
		sprite.height = this._height;
		sprite.scale.copyFrom(this.scale);
		return sprite;
	}
}
export default SuperSprite;
expose("SuperSprite", SuperSprite);

export const load_promises: Promise<any>[] = [];
export class AnimationFrame {
	texture: Texture;
	total_dt: number;
	dt: number;
	declare parent: SuperSprite;
	constructor(params: any) {
		if (!("texture" in params)) throw new Error("AnimationFrame without texture");
		if (typeof params.dt !== "number") throw new Error("AnimationFrame without duration");
		this.texture = get_texture();
		const load_promise = get_texture_async(params.texture).then((tex) => {
			this.texture = tex;
			if (this.parent.init_params.pixilate) tex.baseTexture.scaleMode = SCALE_MODES.NEAREST;
		});
		load_promises.push(load_promise);
		this.total_dt = this.dt = params.dt >= 0 ? params.dt : 100000000;
	}
}

const nothing_sprite = new SuperSprite({ id: "nothing" });
nothing_sprite.raw_animation_frames.push(new AnimationFrame({ texture: "/doesnt_exist", dt: -1 }));
delete load_promises[load_promises.length-1];
nothing_sprite.raw_animation_frames[0].texture = Texture.from("/doesnt_exist");
SuperSprite.sprites.set("nothing", nothing_sprite);

export async function load_all_the_things() {
	const parser = new XmlParser([SuperSprite, AnimationFrame]);

	const res = await fetch("/sprites.txt");
	const sprites_list = (await res.text()).trim().split("\n");
	for (const sprite_url of sprites_list) {
		const sprite_res = await fetch(sprite_url.trim());
		const sprite_definition = await sprite_res.text();
		const sprite = parser.parse_from_text(sprite_definition);
		if (!(sprite instanceof SuperSprite)) throw new Error("Super sprite spriten't");
		if (sprite.id == null || sprite.id.trim() == "") throw new Error("Super sprite idn't");
		SuperSprite.sprites.set(sprite.id, sprite);
	}

	for (const promise of load_promises) await promise;
	//await new Promise(resolve => setTimeout(resolve, 2250));
}
